/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file enumstr.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
#include <stdio.h>
#include <stdlib.h>

extern const char* GenderToString(enum gender gender) ;

extern const char* BreedToString( enum breed breed) ;

extern const char* ColorToString( enum color color) ;