/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//





#pragma once



#include "catDatabase.h"
#include <stdio.h>
#include <stdlib.h>


extern void printCat(int index);

extern void printAllCats();

extern int findCat( const char* name);
