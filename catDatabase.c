/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file CatDatabase.c
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//



#include <stdio.h>
#include <stdbool.h>
#include "catDatabase.h"





struct cat cats[MAX_CAT];
size_t sizearray = sizeof(cats) ;
size_t size = 0 ;
