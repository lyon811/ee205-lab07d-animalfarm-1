/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//







#pragma once


#include "catDatabase.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

extern int updateCatName(int index, const char* newName);
extern bool fixCat(int index);
extern float updateCatWeight( int index, float newWeight);
extern int updateCatCollar1 (int index, int newColor);
extern int updateCatCollar2 (int index, int newColor);
extern int updateLicense( int index, unsigned long long newLicense);