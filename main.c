/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
/// USAGE main
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//






#include <stdio.h>
#include <stdlib.h> // For EXIT_SUCCESS / EXIT_FAILURE
#include <assert.h> // For assert()
#include "config.h"
#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"
#define MAX_NAME1 "1234567890123456789012345678901234567890123456789"
#define MAX_NAME2 "DIFFERENT 123456789012345678901234567890123456789"
#define ILLEGAL_NAME "12345678901234567890123456789012345678901234567890"
int main() {
printf( "Starting %s\n", PROGRAM_NAME ) ;
addCat( "Loki", MALE, PERSIAN, true, 8.5, BLACK, WHITE, 101 ) ;
addCat( "Milo", MALE, MANX, true, 7.0, BLACK, RED, 102 ) ;
addCat( "Bella", FEMALE, MAINE_COON, true, 18.2, BLACK, BLUE, 103 ) ;
addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2, BLACK, GREEN, 104 ) ;
addCat( "Trin", FEMALE, MANX, true, 12.2, BLACK, PINK, 105 ) ;
addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0, WHITE, BLACK, 106 ) ;
printAllCats() ;
int kali = findCat( "Kali" ) ;
assert( updateCatName( kali, "Chili" ) == false ) ; // duplicate cat name should fail
printCat( kali ) ;
updateCatName( kali, "Capulet" ) ;
updateCatWeight( kali, 9.9 ) ;
fixCat( kali ) ;
updateCatCollar1( kali, GREEN )  ;
updateCatCollar2( kali, GREEN ) ;
updateLicense( kali, 201 )  ;
printCat( kali ) ;
printAllCats() ;
deleteAllCats() ;
printAllCats() ;
printf( "Done with %s\n", PROGRAM_NAME ) ;
return( EXIT_SUCCESS ) ;
}