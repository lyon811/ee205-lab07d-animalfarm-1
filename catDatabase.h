/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animalfarm 1  - EE 205 - Spr 2022
///
/// @file CatDatabase.h
/// @version 1.0
///
/// @author Lyon Singleton <lyonws@hawaii.edu>
/// @date 06_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//





#pragma once




#include <stdio.h>
#include <stdbool.h>

#define MAX_CAT 1024
#define MAX_CAT_NAME 50

 enum gender{UNKNOWN_GENDER, MALE, FEMALE}; 
 enum breed{UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
 enum color{BLACK, WHITE, RED, BLUE, GREEN, PINK};

struct cat {
   const char* name ;
   enum gender gender; 
   enum breed breed;
   enum color collarColor1;
   enum color collarColor2;
   unsigned long long license;
   bool isfixed;
   float weight;
};

extern struct cat cats[MAX_CAT];
extern size_t size ;
extern size_t sizearray ;

